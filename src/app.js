const express = require('express');
const morgan = require('morgan');

const agents_routes = require("./routes/agents_routes");

const app = express();

app.use(morgan('dev'));

app.use('/api', agents_routes);

module.exports = app;
