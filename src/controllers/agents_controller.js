const { db } = require('../firebase');
const { validationResult } = require("express-validator");


const addAgent = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            //console.log(errors.array())
            return res.status(400).json({ errors: errors.array() });
        }
        const data = req.body;
        await db.collection("Personajes").doc().set(data);
        res.send("El usuario fue creado correctamente");
        return res.status(204).json();
    } catch (error) {
        console.log(error);
        return res.status(500).send(error);
    }
};

const getAgent = async (req, res) => {
    try {
        const id = req.params.idAgent;
        const Agent = db.collection("Personajes").doc(id);
        const data = await Agent.get();
        if (!data.exists) {
            res.status(404).send("El agente no fue encontrado");
        } else {
            const response = data.data();
            //console.log(response)
            return res.status(200).json(response);
        }
    } catch (error) {
        return res.status(500).json();
    }
};


const getAllAgents = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            //console.log(errors.array())
            return res.status(400).json({ errors: errors.array() });
        }
        const query = db.collection("Personajes");
        console.log(query)
        const querySnapshot = await query.get();
        const docs = querySnapshot.docs;
        const response = docs.map((doc) => ({
            id: doc.id,
            ...doc.data(),
        }));
        return res.status(200).json(response);
    } catch (error) {
        return res.status(500).json();
    }
};

const deleteAgent = async (req, res) => {
    try {
        const id = req.params.idAgent;
        const doc = db.collection("Personajes").doc(id);
        await doc.delete();
        return res.status(200).send("El agente fue borrado con exito");
    } catch (error) {
        return res.status(500).json();
    }
};

const updateAgent = async (req, res) => {
    try {
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
            console.log(errors.array());
            return res.status(400).json({ errors: errors.array() });
        }
        const id = req.params.idAgent;
        const data = req.body;
        const Agent = db.collection("Personajes").doc(id);
        await Agent.update(data);
        res.send("El agente fue actualizado correctamente");
        return res.status(200).json();
    } catch (error) {
        return res.status(500).json();
    }
};



module.exports = {
    addAgent,
    getAllAgents,
    getAgent,
    deleteAgent,
    updateAgent
}
