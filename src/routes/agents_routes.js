const express = require('express');
const { addAgent, getAgent, getAllAgents, deleteAgent, updateAgent } = require('../controllers/agents_controller');
const { body } = require("express-validator");

const router = express.Router({ mergeParams: true });

const AgentCreationValidators = [

    body('name').notEmpty().isString(),
    body('abilitys').notEmpty().isArray(),
    body().custom(body => {
        const keys = ['name', 'abilitys'];
        return Object.keys(body).every(key => keys.includes(key));
    }).withMessage('Se agregaron parametros extra')
]

const AgentUpdateValidators = [
    body("name").notEmpty().isString(),
    body("Abilitys").notEmpty().isArray(),
    body()
        .custom((body) => {
            const keys = ["name", "Abilitys"];
            return Object.keys(body).every((key) => keys.includes(key));
        })
        .withMessage("Se agregaron parametros extra"),
];



router.post('/agents', AgentCreationValidators, addAgent);
router.get('/agents/:idAgent', getAgent);
router.get('/agents', getAllAgents);
router.delete('/agents/:idAgent', deleteAgent);
router.put('/agents/:idAgent', AgentUpdateValidators, updateAgent);

module.exports = router;
